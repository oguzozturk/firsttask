from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView, FormView
from django.core.urlresolvers import reverse_lazy
from pip.index import Link

from books_cbv.forms import ChoiceForm
from books_cbv.models import Question
from books_cbv.models import Choice

class QuestionList(generic.ListView):
    template_name = "question_list.html"
    context_object_name = 'lastest_question_list'

    def get_queryset(self):
        return Question.objects.order_by('-id')[:5]

class DetailView(generic.DetailView,generic.View):
    model = Question
    template_name = 'books_cbv/detail.html'

class QuestionCreate(CreateView):
    model = Question
    fields = ['question_name', 'question_text']
    success_url = reverse_lazy('question_list')

class QuestionUpdate(UpdateView):
    model = Question
    fields = ['question_name', 'question_text']
    success_url = reverse_lazy('question_list')

class QuestionDelete(DeleteView):
    model = Question
    success_url = reverse_lazy('question_list')

class ChoiceCreate(generic.View):
    template_name = "choice_form.html"
    form_class = ChoiceForm()
    fields = 'question_id'

    def get(self, request,pk):
        choice_form = ChoiceForm()
        return render(request,"books_cbv/choice_form.html",
                      {'choice_form':choice_form})

    def post(self,request,pk):
        choice_form = ChoiceForm(data=request.POST)

        if choice_form.is_valid():
            Choice.objects.create(question_id = int(pk),choice_text = choice_form.data.get('choice_text'))
            return HttpResponseRedirect('/question/%s/' % pk)

        else:
            return render(request,'books_cbv/choice_form.html',{'choice_form':choice_form})

class VoteView(generic.View):
    model = Question

    def get(self,request,pk,id):
        choice = Choice.objects.get(id=pk)
        choice.votes+=1
        choice.save()
        return HttpResponseRedirect('/question/%s' % id)

class ChoiceDelete(DeleteView):
    model = Choice

    def delete(self, request, *args, **kwargs):
        """
        Calls the delete() method on the fetched object and then
        redirects to the success URL.
        """
        self.object = self.get_object()
        question_id = self.object.question_id
        self.object.delete()
        return HttpResponseRedirect('/question/%s/' % question_id)