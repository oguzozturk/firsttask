from django.contrib import admin
from books_cbv.models import Question,Choice

class QuestionAdmin (admin.ModelAdmin):
    list_display = ('question_name','question_text')
    search_fields = ['question_name']

admin.site.register(Question, QuestionAdmin)

admin.site.register(Choice)

