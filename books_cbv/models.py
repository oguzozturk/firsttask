from django.db import models
from django.core.urlresolvers import reverse


class Question(models.Model):
    question_name = models.CharField(max_length=200)
    question_text = models.CharField(max_length=200)

    def __str__(self):
        return self.question_name

    def __unicode__(self):
        return self.question_name

    def get_absolute_url(self):
        return reverse('books_cbv:question_edit', kwargs={'pk': self.pk})


class Choice (models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=100)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

    def __unicode__(self):
        return self.choice_text

    def get_absolute_url(self):
        return reverse('books_cbv:choice_edit', kwargs={'pk': self.pk})