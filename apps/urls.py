from django.conf.urls import url, include
from django.contrib import admin

from books_cbv import views


urlpatterns = [
    url(r'^$',views.QuestionList.as_view(), name='question_list'),
    url(r'^admin/', admin.site.urls),
    url(r'^question/(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),

    url(r'^new$', views.QuestionCreate.as_view(), name='question_new'),
    url(r'^edit/(?P<pk>\d+)$', views.QuestionUpdate.as_view(), name='question_edit'),
    url(r'^delete/(?P<pk>\d+)$', views.QuestionDelete.as_view(), name='question_delete'),

    url(r'^question/(?P<pk>[0-9]+)/newchoice/$', views.ChoiceCreate.as_view(), name='choice_new'),
    url(r'^question/(?P<id>[0-9]+)/(?P<pk>[0-9]+)/$', views.VoteView.as_view(), name='vote'),
    url(r'^delete_choice/(?P<pk>\d+)$', views.ChoiceDelete.as_view(), name='choice_delete'),
    url(r'^$', 'apps.views.home'),
]
